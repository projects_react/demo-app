export default [
    {
        title: 'Home',
        utl: '/'
    },
    {
        title: 'Information',
        url: '/information'
    },
    {
        title: 'Task',
        url: '/task'
    },
    {
        title: 'Contact Us',
        url: '/contact'
    }
];