import React, { Component } from 'react';
import './css/App.css';

// Components

import Navbar from '../layout/Navbar';
import Content from '../layout/Content';
import Footer from '../layout/Footer';

// Data

import items from '../data/menu';

class App extends Component {
  render() {
    return <div className="App">
        <Navbar title="Hola desde react" items={items} />
        <Content />
        <Footer copyright="&copy; copyRight 2018"/>
      </div>;
  }
} 

export default App;
