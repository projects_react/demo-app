import React, { Component } from 'react';
import logo from './../components/images/logo.svg';
import './../components/css/Navbar.css';

import PropTypes from 'prop-types';

class Navbar extends Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        items: PropTypes.array.isRequired
    };
    render() {
        console.log('---> props --- >', this.props);
        // const title = this.props.title;
        // const items = this.props.items;
        const { title, items } = this.props;
        return (
            <div className='Navbar'>
                <div className='Logo'>
                    <img src={logo} className='Logo' alt='logo' />
                    <h1 className='Navbar-title'>Welcome to React</h1>
                    <h2>
                        {title}
                    </h2>
                    <ul className='Menu'>
                        {
                            items && items.map(
                                (item, key) => <li key={key}>{item.title}</li>
                            )
                        }
                    </ul>
                </div>
            </div>
        );
    }
}

export default Navbar;
