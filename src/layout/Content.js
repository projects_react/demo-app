import React, { Component } from 'react';
import './../components/css/Content.css';

class Content extends Component {
  render() {
    return (
      <div className="Content">
        <h1>Contenido</h1>
        <p>Hola desde React</p>
      </div>
    );
  }
}

export default Content;
