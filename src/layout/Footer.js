import React, { Component } from 'react';
import './../components/css/Footer.css';

import PropTypes from "prop-types";

class Footer extends Component {
  static propTypes = {
    copy: PropTypes.string
  };
  render() {
      const copyright = this.props.copyright;
    return (
      <div className="Footer">
            <p>{copyright}</p>
      </div>
    );
  }
}

export default Footer;